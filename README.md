# Canvas Image Resizer

Convert HTML image to data URL - File, for compression and resizing. Is pure JS and Apis from browser. Note: Only browser

## Getting started

Module return a function, and you can set properties:

```javascript
import Resizer from "canvas-image-resizer";

// ...
// img is HTML Image DOM element

const base64 = Resizer(img, {
  width: 1000, // Integer of maximus width of image, in pixels
  height: 1000, // Integer of maximus height of image, in pixels
  quality: 1, // Decimal, 0 to 1 values
})
// ...
```
